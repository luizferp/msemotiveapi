from emotive import Emotive
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--image')
    #parser.add_argument('--video')
    args = parser.parse_args()

    em = Emotive(Emotive.MSFaceAPI, "<ms-subscription-key>")
    res = em.detect_emotion(args.image)
    print(res)

#    gvision = GoogleCloudVisionAPI()
#    gvision.run()
